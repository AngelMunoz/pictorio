import { autoinject, bindable } from 'aurelia-framework';
import { EventAggregator, Subscription } from 'aurelia-event-aggregator';
import { IContextMenuItem } from 'interfaces/IContextMenuItem';
import { EventCodes } from 'enums/event-codes';
import { IOpenContextMenuArgs } from 'interfaces/IOpenContextMenuArgs';


@autoinject
export class PicContextMenu {
  @bindable title: string;
  @bindable menuClass: string = 'has-background-dark has-text-light'
  @bindable coordinates: { x: number, y: number } = { x: 0, y: 0 };
  @bindable options: IContextMenuItem[] = [];

  isOpen = false;
  currentCoordinates: { top: string | number, left: string | number } = { top: 0, left: 0 };
  subscriptions: Subscription[] = [];

  constructor(private ea: EventAggregator, private el: Element) {
    const sub = this.ea.subscribe(EventCodes.OPEN_CONTEXT_MENU, this.onOpenContextMenu.bind(this));
    const sub1 = this.ea.subscribe(EventCodes.CLOSE_CONTEXT_MENU, () => this.isOpen = false);
    this.subscriptions.push(sub, sub1);
  }

  coordinatesChanged({ x, y }, oldValue) {
    this.currentCoordinates = {
      left: `${this.coordinates.x}px`,
      top: `${this.coordinates.y}px`
    }
  }

  onOpenContextMenu({ coordinates, items, title }: IOpenContextMenuArgs) {
    if (title) {
      this.title = title;
    }

    if (coordinates) {
      this.coordinates = coordinates;
    }

    if (items) {
      this.options = items;
    }

    this.currentCoordinates = {
      left: `${this.coordinates.x}px`,
      top: `${this.coordinates.y}px`
    }
    this.isOpen = true;
  }

  close() {
    this.isOpen = false;
  }

  callFn(action: CallableFunction, eventData: Record<string, any>) {
    this.close();
    action(eventData);
    return;
  }

  detached() {
    for (const subscription of this.subscriptions) {
      subscription.dispose();
    }
  }
}
