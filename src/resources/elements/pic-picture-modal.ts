import { bindable, autoinject } from 'aurelia-framework';

@autoinject
export class PicPictureModal {
  @bindable src: string;
  @bindable isOpen = false;

  constructor(private el: Element) { }

  close() {
    const event = new CustomEvent('on-pic-modal-closing', { bubbles: true, composed: true });
    this.el.dispatchEvent(event);
  }
}
