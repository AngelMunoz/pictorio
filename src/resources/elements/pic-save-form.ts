import { bindable, autoinject, observable } from 'aurelia-framework'

@autoinject
export class PicSaveForm {
  @bindable src: string;
  @bindable index: string | number;


  isInvalid: boolean;

  @observable
  picType: '' | 'selfie' | 'documento' | 'familiar' = '';

  constructor(private el: Element) { }

  picTypeChanged(newValue: '' | 'selfie' | 'documento' | 'familiar', oldValue: '' | 'selfie' | 'documento' | 'familiar') {
    if (newValue !== '') { this.isInvalid = false; }
  }

  private dispatchEvent(name: string) {
    const event = new CustomEvent<Record<string, string | number>>(name, {
      bubbles: true,
      composed: true,
      cancelable: true,
      detail: {
        src: this.src,
        index: this.index,
        type: this.picType
      }
    });
    this.el.dispatchEvent(event);
  }

  public see() {
    this.dispatchEvent('on-pic-see');
  }

  public send(form: HTMLFormElement) {
    if (!form.checkValidity()) {
      this.isInvalid = true
      return;
    }
    this.dispatchEvent('on-pic-send');
  }

  public discard() {
    this.dispatchEvent('on-pic-discard')
  }

}
