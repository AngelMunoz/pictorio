import { FrameworkConfiguration, PLATFORM } from 'aurelia-framework';

export function configure(config: FrameworkConfiguration) {
  config.globalResources([
    PLATFORM.moduleName('./elements/pic-save-form'),
    PLATFORM.moduleName('./elements/pic-picture-modal'),
    PLATFORM.moduleName('./elements/pic-context-menu')
  ]);
}
