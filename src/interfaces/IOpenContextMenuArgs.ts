import { IContextMenuItem } from "./IContextMenuItem";

export interface IOpenContextMenuArgs {
  title?: string;
  coordinates?: { x: number, y: number };
  items?: IContextMenuItem[];
}