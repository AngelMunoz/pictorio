export interface IContextMenuItem {
  label: string;
  action?: CallableFunction;
  eventData?: Record<string, any>
}