import { delay } from "utils";

export class MediaService {

  public readonly defaultMediaStreamConstraints: MediaStreamConstraints = { video: true };

  private stream: MediaStream;
  private cameras: Array<MediaDeviceInfo & { active?: boolean }> = [];

  private lastActiveCamera: string;

  get supportsUserMedia() {
    return navigator.mediaDevices && navigator.mediaDevices.getUserMedia;
  }

  get supportsEnumerateDevices() {
    return navigator.mediaDevices && navigator.mediaDevices.enumerateDevices;
  }

  get hasMultipleCameras() {
    return this.cameras.length > 1;
  }

  getActiveCameras() {
    if (!this.stream) return [];
    const tracks = this.stream.getVideoTracks();
    return tracks.filter(track => track.enabled);
  }

  getInactiveCameras() {
    return this.cameras.filter(camera => !camera.active);
  }

  private async setActiveCamera() {
    const active = this.getActiveCameras();
    this.lastActiveCamera = active[0] && active[0].label;
    this.cameras = await this.getVideoDevices();
    this.cameras = this.cameras.map(camera => {
      const thisCamera = active.find(track => track.label === camera.label);
      if (!thisCamera) {
        camera.active = false;
      }
      else {
        camera.active = thisCamera.enabled;
      }
      return camera;
    });
  }

  async requestPermission() {
    if (!this.supportsUserMedia) throw new Error('The Browser does not support getUserMedia');
    try {
      await navigator.mediaDevices.getUserMedia({ video: true });
      return true;
    } catch (error) {
      console.warn(error.message);
      return false;
    }
  }

  async getVideoDevices() {
    if (!this.supportsEnumerateDevices) throw new Error('The browser does not support enumerateDevices');
    try {
      const devices: MediaDeviceInfo[] = await navigator.mediaDevices.enumerateDevices();
      return devices.filter(device => device.kind == "videoinput");
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async startCamera(video: HTMLVideoElement, constraints = this.defaultMediaStreamConstraints, retryCount = 10) {
    if (!this.supportsUserMedia) throw new Error('The Browser does not support getUserMedia');
    try {
      const stream = await navigator.mediaDevices.getUserMedia(constraints);
      this.stream = stream;
      video.srcObject = this.stream;
    } catch (error) {
      if (retryCount > 0) {
        console.warn(`Error ${error.message}... retrying once more`);
        await delay(750);
        return this.startCamera(video, constraints, retryCount - 1);
      }
      return Promise.reject(error);
    }
    return this.setActiveCamera();
  }

  async switchCamera(source: HTMLVideoElement, deviceId?: string) {
    try {
      await this.stopCamera(source);
    } catch (error) {
      return Promise.reject(error);
    }
    try {
      if (deviceId) {
        await this.startCamera(source, { video: { deviceId: { exact: deviceId } } });
      } else {
        const inactive = this.getInactiveCameras();
        const [camera] = inactive.filter(camera => camera.label !== this.lastActiveCamera);
        await this.startCamera(source, { video: { deviceId: { exact: camera.deviceId } } });
      }
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async stopCamera(source?: HTMLVideoElement) {
    if (source.srcObject) {
      for (const track of (source.srcObject as MediaStream).getTracks()) {
        track.stop();
      }
    } else {
      for (const track of this.stream.getTracks()) {
        track.stop();
      }
      this.stream = null;
    }
    if (this.stream && source && source.srcObject && (this.stream.id === (source.srcObject as MediaStream).id)) {
      this.stream = null;
    }
    source.pause();
    source.srcObject = null;
  }

  takeScreenshot(source: HTMLVideoElement, drawArea: HTMLCanvasElement, format = 'image/png') {
    drawArea.width = source.videoWidth;
    drawArea.height = source.videoHeight;
    drawArea.getContext('2d').drawImage(source, 0, 0);
    return drawArea.toDataURL(format);
  }
}