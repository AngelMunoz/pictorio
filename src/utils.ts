export const delay = (timeout = 500) => new Promise(resolve => setTimeout(() => resolve(), timeout));


export async function base64PngToFile(url: string, name: string, mimeType = 'image/png'): Promise<File> {
  return fetch(url)
    .then(res => res.arrayBuffer())
    .then(buffer => new File([buffer], name, { type: mimeType }));
}