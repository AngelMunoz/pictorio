import { autoinject } from "aurelia-framework";
import { RedirectToRoute, Router } from 'aurelia-router';
import { ErrorCodes } from 'enums/error-codes';
import { base64PngToFile, delay } from 'utils';

@autoinject
export class Pictures {
  pictures: string[] = [];

  isShowingPicture = false;
  currentSrc: string;

  constructor(private router: Router) { }

  cleanCurrentPicture() {
    this.isShowingPicture = false;
    this.currentSrc = '';
  }

  onPicSee(event: CustomEvent<Record<string, string | number>>) {
    this.currentSrc = event.detail.src as string;
    this.isShowingPicture = true;
  }

  async onPicSend(event: CustomEvent<Record<string, string | number>>) {
    const file = await base64PngToFile(event.detail.src as string, `image-${event.detail.index}.png`);
    console.log(file.name, file.type, file.size);
    await delay(1500);
    this.saveToLocalStorage(event.detail.src as string);
    this.onPicDiscard(event);
  }


  onPicDiscard({ detail }: CustomEvent<Record<string, string | number>>) {
    this.pictures = this.pictures.filter((picture, index) => index != detail.index);
    sessionStorage.setItem('pictureSet', JSON.stringify(this.pictures));
    if (this.pictures.length <= 0) this.router.navigateToRoute('home');
  }

  saveToLocalStorage(src: string) {
    const json = localStorage.getItem('savedPictures') || "[]";
    const savedPictures = JSON.parse(json);
    savedPictures.push(src);
    localStorage.setItem('savedPictures', JSON.stringify(savedPictures));
  }

  canActivate(params, routeConfig, navigationInstruction) {
    const pictureSet = sessionStorage.getItem('pictureSet')
    if (!pictureSet) return new RedirectToRoute('home', { code: ErrorCodes.INVALID_SET }, { replace: true });
    this.pictures = JSON.parse(pictureSet);
    if (this.pictures.length <= 0) return new RedirectToRoute('home', { code: ErrorCodes.EMPTY_SET }, { replace: true });
    return true;
  }

}