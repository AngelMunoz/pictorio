import { autoinject } from 'aurelia-framework'
import { ErrorCodes } from 'enums/error-codes';
import { EventAggregator } from 'aurelia-event-aggregator';
import { EventCodes } from 'enums/event-codes';
import { IOpenContextMenuArgs } from 'interfaces/IOpenContextMenuArgs';
@autoinject
export class Home {

  pictures: string[] = [];
  message: string;
  currentSrc: string;
  inError = false;
  isShowingPicture = false;

  constructor(private ea: EventAggregator) { }

  activate({ code }: { [key: string]: any, code?: ErrorCodes }) {
    this.setMessage(Number(code));
    this.getImages();
  }

  setMessage(code: ErrorCodes) {
    this.inError = true;
    switch (code) {
      case ErrorCodes.UNSUPPORTED:
        this.message = "Tu navegador no admite algunas funcionalidades necesarias en el sitio";
        break;
      case ErrorCodes.PERMISSION_DENIED:
        this.message = "Elegiste negar el acceso a la camara a este sitio, intenta nuevamente dando permisos para el uso de la camara";
        break;
      case ErrorCodes.INVALID_SET:
        this.message = "No se guardaron bien los datos de las imagenes, por favor limpia los datos de el navegador e intenta nuevamente";
        break;
      case ErrorCodes.EMPTY_SET:
        this.message = "No hay fotos pendientes por revisar";
        break;
      default:
        this.inError = false;
        break;
    }
  }

  getImages() {
    try {
      const json = localStorage.getItem('savedPictures') || "[]";
      const savedPictures = JSON.parse(json);
      this.pictures = savedPictures;
    } catch (error) {
      console.warn(error.message);
      this.pictures = [];
    }
  }

  seePicture(src: string) {
    this.currentSrc = src;
    this.isShowingPicture = true;
  }

  cleanCurrentPicture() {
    this.isShowingPicture = false;
    this.currentSrc = '';
  }

  onContextMenu(event: MouseEvent, index: number) {
    const eventArgs: IOpenContextMenuArgs = {
      title: 'Opciones',
      coordinates: { x: event.pageX, y: event.pageY },
      items: [
        { action: this.onDeleteCalled.bind(this), eventData: { index }, label: 'Borrar Foto' }
      ]
    }
    this.ea.publish(EventCodes.OPEN_CONTEXT_MENU, eventArgs);
  }

  onDeleteCalled({ index }) {
    this.pictures.splice(index, 1);
    localStorage.setItem('savedPictures', JSON.stringify(this.pictures));
  }

}