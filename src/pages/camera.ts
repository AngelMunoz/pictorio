import { autoinject } from 'aurelia-framework';
import { RedirectToRoute, Router } from 'aurelia-router';
import { MediaService } from 'services/media.service';
import { ErrorCodes } from 'enums/error-codes';

@autoinject
export class Camera {
  source: HTMLVideoElement = null;
  drawArea: HTMLCanvasElement = null;

  hasMultipleCameras = false;

  pictures: string[] = [];

  constructor(private $media: MediaService, private router: Router) { }

  async switchCamera() {
    this.$media.switchCamera(this.source);
  }

  takePicture() {
    const src = this.$media.takeScreenshot(this.source, this.drawArea);
    this.pictures.push(src);
  }

  goToUpload() {
    const jsonified = JSON.stringify(this.pictures);
    sessionStorage.setItem('pictureSet', jsonified);
    return this.router.navigateToRoute('pictures');
  }

  cancel() {
    this.pictures = [];
  }

  async attached() {
    try {
      await this.$media.startCamera(this.source, { video: true });
      this.hasMultipleCameras = this.$media.hasMultipleCameras;
    } catch (error) {
      console.warn(error.message);
    }
  }

  async canActivate(params, routeConfig, navigationInstruction) {
    if (!this.$media.supportsEnumerateDevices || !this.$media.supportsUserMedia) {
      return new RedirectToRoute(
        'home',
        { code: ErrorCodes.UNSUPPORTED },
        { replace: true }
      );
    }
    const hasPermission = await this.$media.requestPermission();
    if (!hasPermission) {
      return new RedirectToRoute(
        'home',
        { code: ErrorCodes.PERMISSION_DENIED },
        { replace: true }
      );
    }
    return true;
  }

  deactivate() {
    this.$media.stopCamera(this.source);
  }
}