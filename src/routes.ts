import { PLATFORM } from 'aurelia-pal'
import { RouteConfig } from 'aurelia-router';


export const ROUTES: RouteConfig[] = [
  { route: ['', 'home'], name: 'home', title: 'Inicio', moduleId: PLATFORM.moduleName('./pages/home'), nav: false },
  { route: 'camera', name: 'camera', title: 'Camara', moduleId: PLATFORM.moduleName('./pages/camera'), nav: false },
  { route: 'pictures', name: 'pictures', title: 'Elegir Fotos', moduleId: PLATFORM.moduleName('./pages/pictures'), nav: false },
];