import { RouterConfiguration, Router } from 'aurelia-router';
import { ROUTES } from 'routes';

export class App {
  router: Router;
  isOpen = false;

  configureRouter(config: RouterConfiguration, router: Router) {
    this.router = router;
    config.title = 'Pictorio';
    config.map(ROUTES);
  }

  toggleMenu() {
    this.isOpen = !this.isOpen;
  }
}


